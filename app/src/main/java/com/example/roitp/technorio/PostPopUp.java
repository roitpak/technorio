package com.example.roitp.technorio;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class PostPopUp extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_pop_up);

        Intent i = getIntent();
        String title = i.getStringExtra("title");
        String body = i.getStringExtra("body");

        TextView titleView = (TextView) findViewById(R.id.titleOfPopUp);
        TextView bodyView = (TextView) findViewById(R.id.bodyOfPopUp);

        titleView.setText(title);
        bodyView.setText(body);


    }
}
