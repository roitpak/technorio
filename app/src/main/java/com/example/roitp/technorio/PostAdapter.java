package com.example.roitp.technorio;

import android.app.Activity;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by roitp on 11-Sep-17.
 */

public class PostAdapter extends ArrayAdapter<Post> {
    public PostAdapter(Activity context, ArrayList<Post> posts) {

        super(context, 0, posts);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View listItemView = convertView;
        if(listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(
                    R.layout.list_item, parent, false);
        }


        Post currentPost = getItem(position);


        TextView idTextView = (TextView) listItemView.findViewById(R.id.id);

        idTextView.setText(currentPost.getId());

        TextView headTextView = (TextView) listItemView.findViewById(R.id.head);

        headTextView.setText(currentPost.getTitle());

        TextView bodyTextView = (TextView) listItemView.findViewById(R.id.body);

        bodyTextView.setText(currentPost.getBody());


        return listItemView;
    }
}
