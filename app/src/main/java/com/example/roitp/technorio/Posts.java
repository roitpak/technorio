package com.example.roitp.technorio;


import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import android.os.AsyncTask;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import java.net.MalformedURLException;



public class Posts extends AppCompatActivity {

    Spinner spinner;

    public static final String LOG_TAG = MainActivity.class.getSimpleName();


    private static final String USGS_REQUEST_URL = "http://api.technorio.com/posts";
    String URLHolder;
    Boolean check;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        URLHolder = USGS_REQUEST_URL;
        check = true;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posts);

        spinner = (Spinner)findViewById(R.id.spinner);
        String[] tags = {"ALL","C","Python","Swift","Ruby","Javascript","PHP","C#","Java"};
        ArrayAdapter adapter = new ArrayAdapter(this,R.layout.support_simple_spinner_dropdown_item,tags);
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if(i==0){
                    URLHolder = USGS_REQUEST_URL;
                    check = true;
                    technorioAsyncTask task = new technorioAsyncTask();
                    task.execute();
                    return;
                }
                check = false;
                String generateURL = "http://api.technorio.com/tag/"+i;
                URLHolder = generateURL;
                Log.v(LOG_TAG,"generate URL " +generateURL);
                technorioAsyncTask task = new technorioAsyncTask();
                task.execute();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        technorioAsyncTask task = new technorioAsyncTask();
        task.execute();


    }
    private void updateUi(final ArrayList<Post> posts){
        PostAdapter itemsAdapter = new PostAdapter(this,posts);

        ListView listView = (ListView) findViewById(R.id.list);

        listView.setAdapter(itemsAdapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                int p = position;
                Log.v(LOG_TAG,"p"+p);
                Post tempPost = posts.get(p);
                Intent i = new Intent(Posts.this,PostPopUp.class);
                i.putExtra("title",tempPost.getTitle());
                i.putExtra("body",tempPost.getBody());
                startActivity(i);


            }
        });
    }


    private class technorioAsyncTask extends AsyncTask<URL, Void,ArrayList<Post>> {
        @Override
        protected ArrayList<Post> doInBackground(URL... urls) {
            URL url = createUrl(URLHolder);

            String jsonResponse = "";
            try {
                jsonResponse = makeHttpRequest(url);
            } catch (IOException e) {
               Log.v(LOG_TAG,"IOE exception executed " + e);
            }

            ArrayList<Post> postOfTechnorio = extractFeatureFromJson(jsonResponse);
            return postOfTechnorio;
        }

        @Override
        protected void onPostExecute(ArrayList<Post> post) {
            if(post==null){
                return;
            }
            updateUi(post);
        }
        private URL createUrl(String stringUrl) {
            URL url = null;
            try {
                url = new URL(stringUrl);
            } catch (MalformedURLException exception) {
                Log.e(LOG_TAG, "Error with creating URL", exception);
                return null;
            }
            return url;
        }
        private  String makeHttpRequest(URL url) throws IOException{
            String jsonResponse = "";
            if(url ==null){
                return jsonResponse;
            }
            HttpURLConnection urlConnection = null;
            InputStream inputStream = null;
            try {
                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.setReadTimeout(10000 /* milliseconds */);
                urlConnection.setConnectTimeout(15000 /* milliseconds */);
                urlConnection.connect();

                if(urlConnection.getResponseCode()==200){
                    inputStream = urlConnection.getInputStream();
                    jsonResponse = readFromStream(inputStream);
                }else{
                    Log.e(LOG_TAG,"Response code is not 200 "+urlConnection.getResponseCode() );
                }
            } catch (IOException e) {
                Log.e(LOG_TAG,"IOE exception thrown" + e);
            } finally {
                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (inputStream != null) {

                    inputStream.close();
                }
            }
            return jsonResponse;
        }
        private String readFromStream(InputStream inputStream) throws IOException {
            StringBuilder output = new StringBuilder();
            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
                BufferedReader reader = new BufferedReader(inputStreamReader);
                String line = reader.readLine();
                while (line != null) {
                    output.append(line);
                    line = reader.readLine();
                }
            }
            return output.toString();
        }
        private ArrayList<Post>  extractFeatureFromJson(String postJSON){
            if(TextUtils.isEmpty(postJSON)){
                return null;
            }
            ArrayList<Post> toReturnPost = new ArrayList<>();

            if(check){
                try{

                    JSONArray baseJsonResponse = new JSONArray(postJSON);
                    if(baseJsonResponse.length()>0){
                        for(int i = 0 ; i<baseJsonResponse.length();i++){
                            JSONObject a = baseJsonResponse.getJSONObject(i);
                            String id = a.getString("id");
                            String title = a.getString("title");
                            String body = a.getString("body");

                            toReturnPost.add(new Post(id,title,body));


                        }
                        return toReturnPost;
                    }


                }catch (JSONException e){
                    Log.e(LOG_TAG, "Problem parsing the earthquake JSON results", e);

                }

            }else{
                try{
                    JSONObject root = new JSONObject(postJSON);
                    JSONArray posts = root.getJSONArray("posts");

                    if(posts.length()>0){
                        for(int i = 0 ; i<posts.length();i++){
                            JSONObject a = posts.getJSONObject(i);
                            String id = a.getString("id");
                            String title = a.getString("title");
                            String body = a.getString("body");

                            toReturnPost.add(new Post(id,title,body));


                        }
                        return toReturnPost;
                    }else{
                        toReturnPost.add(new Post("","NO POSTS","NO POSTS"));
                        return  toReturnPost;
                    }


                }catch (JSONException e){
                    Log.e(LOG_TAG, "Problem parsing the earthquake JSON results", e);

                }
            }
            return null;


        }

    }
}
