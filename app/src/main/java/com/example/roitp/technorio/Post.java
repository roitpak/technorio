package com.example.roitp.technorio;

import static android.os.Build.ID;

/**
 * Created by roitp on 11-Sep-17.
 */

public class Post {
    String id;
    String title;
    String body;
    public Post(String ID, String titleOf, String bodyOf){
        id = ID;
        title = titleOf;
        body = bodyOf;

    }
    public String getId(){
        return id;
    }
    public String getTitle(){
        return title;
    }
    public String getBody(){
        return body;
    }
}
